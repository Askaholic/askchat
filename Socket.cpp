#include "Socket.h"
#include <arpa/inet.h>
#include <cstring>
#include <netdb.h>
#include <netinet/in.h>
#include <poll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

int Socket::write(std::string s) {
  if(!open || socketFD < 0) return -1;
  if(::write(socketFD, s.c_str(), s.length()) == -1) {
    open = false;
    return -2;
  }
  return 0;
}

std::string Socket::read(int timeout) {
  if(isOpen()) {
    struct pollfd p;
    p.fd = socketFD;
    p.events = POLLIN;
    p.revents = 0;
    int pR, rR;
    if((pR = ::poll(&p, 1, timeout)) > 0) {
      char buffer[256] = {0};
      if((rR = ::read(socketFD, buffer, sizeof(buffer) - 1)) == 0) {
        open = false;
        return "";
      }
      return std::string(buffer);
    }
  }
  return "";
}

void Socket::close() {
  if(isOpen()) {
    ::close(socketFD);
    open = false;
  }
}

int ClientSocket::create(std::string domain) {
  //Check that the socket isn't listening already
  if(open) return 0;
  //Check ifhe port is valid
  if(port < 0 || port > 65535) return -1;

  struct addrinfo hints, *p;
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;

  if(inet_pton(AF_INET, domain.c_str(), &serverAddress) == 1) {
    hints.ai_flags = AI_CANONNAME;
  }
  int err;
  if((err = getaddrinfo(domain.c_str(), std::to_string(port).c_str(), &hints, &serverInfo) != 0)) {
    open = false;
    return err - 100;
  }
  for(p = serverInfo; p != NULL; p = p->ai_next) {
    if ((socketFD = socket(p->ai_family, p->ai_socktype,p->ai_protocol)) == -1) {
      //The socket failed, so try the next one
      open = false;
      continue;
    }
    if(connect(socketFD, p->ai_addr, p->ai_addrlen) == -1) {
      ::close(socketFD);
      open = false;
      continue;
    }
    break;
  }
  if(p == NULL) {
    //None of the given addresses worked;
    freeaddrinfo(serverInfo);
    open = false;
    return -2;
  }
  else {
    open = true;
    freeaddrinfo(serverInfo);
    return 0;
  }
}

int ServerSocket::create() {
  //Check that the socket isn't listening already
  if(open) return 0;
  //Check if the port is valid
  if(port < 0 || port > 65535) return -1;

  struct addrinfo hints, *p;
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  if(int err = getaddrinfo(NULL, std::to_string(port).c_str(), &hints, &serverInfo) != 0) {
    open = false;
    return err - 100;
  }
  for(p = serverInfo; p != NULL; p = p->ai_next) {
    //Get a file descriptor for the new socket
    if((socketFD = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      open = false;
      continue;
    }
    if(bind(socketFD, p->ai_addr, p->ai_addrlen) == -1) {
      //Bind failed. Should never happen because it only fails when the socket is incorrect
      ::close(socketFD);
      open = false;
      continue;
    }
    break;
  }
  if(p == NULL) {
    //The socket failed
    freeaddrinfo(serverInfo);
    open = false;
    return -2;
  }
  else {
    //Finally start listening
    listen(socketFD, 5);
    freeaddrinfo(serverInfo);
    open = true;
    return 0;
  }
}

ServerSocket* ServerSocket::accept() {
  //Create needs to be called first
  if(!open) return NULL;

  unsigned int l = sizeof(clientAddress);
  int childFD = ::accept(socketFD, (struct sockaddr *) &clientAddress, &l);
  //Something strange happened
  if(childFD < 0) return NULL;

  return new ServerSocket(port, true, childFD);
}

std::string ServerSocket::clientIP() {
  if(!isOpen()) return "";
  char address[INET_ADDRSTRLEN];
  inet_ntop(AF_INET, &(clientAddress.sin_addr), address, INET_ADDRSTRLEN);
  return std::string(address);
}
