#ifndef OPTIONS_H
#define OPTIONS_H

#include <string>
#include <vector>

class Option {
public:
  Option(std::string n) {name = n;}
  Option(std::string n, int v) {name = n; value = v;}
  std::string name;
  int value;
};

class Options {
private:
  bool sorted = true;

  std::vector<Option> options;
public:
  Options(std::vector<Option>);
  int setOption(std::string, int);
  int getOption(std::string);
  bool isSorted() {return sorted;}
};
#endif
