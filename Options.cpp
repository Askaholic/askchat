#include "Options.h"
#include <vector>

Options::Options(std::vector<Option> opt) {
  options.push_back(opt[0]);
  for(int c = 1; c < opt.size(); c++) {
    if(opt[c - 1].name > opt[c].name) {
      //The vector is not sorted correctly
      sorted = false;
    }
    options.push_back(opt[c]);
  }
}

int Options::setOption(std::string name, int value) {
  bool exists = false;
  if(isSorted()) {
    //Binary search
    int l = 0;
    int r = options.size() - 1;
    int h = (r - l) / 2;

    do {
      if(options[h].name == name) {
        options[h].value = value;
        exists = true;
        break;
      }
      else if (options[h].name < name) {
        l = h + 1;
        h = l + ((r - l) / 2);
      }
      else {
        r = h;
        h = l + ((r - l) / 2);
      }
    } while(l != r);
    if(options[h].name == name) {
        options[h].value = value;
        exists = true;
    }
    else if(!exists) {
      options.push_back(Option(name, value));
      if(options[options.size() - 2].name > name) {
        sorted = false;
      }
    }
  }
  else {
    //linear search
    for(int c = 0; c < options.size(); c++) {
      if(options[c].name == name) {
        options[c].value = value;
        exists = true;
        break;
      }
    }
    if(!exists) {
      options.push_back(Option(name, value));
    }
  }
}

int Options::getOption(std::string name) {
  if(isSorted()) {
    //Binary search
    int l = 0;
    int r = options.size() - 1;
    int h = (r - l) / 2;

    do {
      if(options[h].name == name) {
        return options[h].value;
      }
      else if (options[h].name < name) {
        l = h + 1;
        h = l + ((r - l) / 2);
      }
      else {
        r = h;
        h = l + ((r - l) / 2);
      }
    } while(l != r);
    if(options[h].name == name) {
      return options[h].value;
    }
  }
  else {
    //linear search
    for(int c = 0; c < options.size(); c++) {
      if(options[c].name == name) {
        return options[c].value;
      }
    }
  }
  return -1;
}
