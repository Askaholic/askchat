#include "Parser.h"
#include <algorithm>
#include <array>
#include <cstdlib>
#include <functional>
#include <string>
#include <vector>

std::vector<std::string> Parser::tokenize(std::string s) {
  s = removeSpace(s);
  int length = s.length();
  std::vector<std::string> v;
  int prevS = 0;
  for(int c = 0; c < length; c++) {
    if(s.at(c) == ':' && prevS != c) {
      v.push_back(s.substr(prevS, c - prevS));
      prevS = c + 1;
    }
  }
  v.push_back(s.substr(prevS, length - prevS));
  return v;
}

std::string Parser::removeSpace(std::string s) {
  int length = s.length();
  for(int c = 0; c < length; c++) {
    if(s.at(c) == ' ') {
      s.erase(c, 1);
      c--;
      length--;
    }
  }
  return s;
}
