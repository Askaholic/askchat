#ifndef SOCKET_H
#define SOCKET_H

#include <netinet/in.h>
#include <string>

class Socket {
protected:
  int port;
  int socketFD;
  struct sockaddr_in serverAddress;
  struct addrinfo *serverInfo;

  bool open = false;
public:
  int write(std::string);
  std::string read(int);
  void close();

  bool isOpen() {return open;}
  int getPort() {return port;}
};

class ClientSocket : public Socket {
public:
  ClientSocket(int p) {port = p;}
  int create(std::string);
  void accept();
};

class ServerSocket : public Socket {
private:
  struct sockaddr_in clientAddress;
public:
  ServerSocket(int p) {port = p;}
  ServerSocket(int p, bool o, int sfd) {port = p; open = o; socketFD = sfd;}
  int create();
  ServerSocket* accept();
  std::string clientIP();
};

#endif
