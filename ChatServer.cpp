#include "Options.h"
#include "Parser.h"
#include "Socket.h"
#include <boost/chrono.hpp>
#include <boost/thread.hpp>
#include <fstream>
#include <iostream>
#include <netdb.h>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::fstream;
using std::string;
using std::vector;

static bool isRunning = false;

void serverThreadWorker(int, ServerSocket*, bool*, bool*, string);
void serverThreadMain(int, int);
void serverThreadHandler(ServerSocket*);

int main() {
  vector<Option> defaultOptions = {Option("maxUsers", 20), Option("port", 3705) };
  Options options(defaultOptions);
  //Read the config file
  ifstream config;
  config.open("askchat.conf", std::ios::in);
  if(config.is_open()) {
    string next;
    int lineN = 0;
    Parser p;
    while(!config.eof()) {
      lineN++;
      getline(config, next);
      if(next == "") {
        continue;
      }
      vector<string> tokens = p.tokenize(next);
      //Set the options found in the config file
      if(tokens.size() == 2) {
        cout << tokens[0] << " = " << tokens[1] << endl;
        options.setOption(tokens[0], atoi(tokens[1].c_str()));
      }
      else {
        cout << "Error on line " << lineN << endl;
        cout << "Option " << tokens[0] << " has incorrect arguments" << endl;
      }
    }
    config.close();
  }
  //The config file doesn't exist so create it with some defaults
  else {
    cout << "Could not open file" << endl;
    ofstream configOut("askchat.conf");
    if(configOut.is_open()) {
      for(int c = 0; c < defaultOptions.size(); c++) {
        configOut << defaultOptions[c].name << ":" << defaultOptions[c].value << endl;
      }
    }
    else {
      cout << "Unable to create config file\n";
    }
  }

  //Listen on the port and spawn off threads as connections come in
  int arg1 = options.getOption("port");
  int arg2 = options.getOption("maxUsers");
  boost::thread sMain(serverThreadMain, arg1, arg2);
  sMain.join();
  return 0;
}

void serverThreadMain(const int port, const int maxUsers) {
  isRunning = true;
  boost::thread* workers[maxUsers];
  ServerSocket* sockets[maxUsers];
  int workersIndex = 0;
  bool idle[maxUsers] = {true};
  bool kill[maxUsers] = {false};

  for(int c = 0; c < maxUsers; c++) {
    workers[c] = new boost::thread(serverThreadWorker, c, sockets[c], &kill[c], &idle[c], "Welcome to Ask's chat server\n");
  }

  cout << "Starting server\n";
  ServerSocket socket(port);
  int err;
  if((err = socket.create()) != 0) {
    cout << "Server failed to start with code " << err;
    if(err < 100) {
      cout << gai_strerror(err + 100) << endl;
    }
  }

  if(!socket.isOpen()) {
    cout << "WTF? WHY ISNT THE SOCKET OPEN?\n";
    exit(-1);
  }
  while(isRunning) {
    ServerSocket* newSocket = socket.accept();
    //Find the next idle thread
    for(; idle[workersIndex]; workersIndex = (workersIndex + 1) % maxUsers) {}
    sockets[workersIndex] = newSocket;
    workers[workersIndex]->interrupt();
    //Spawn off a new thread to handle the connection
    // boost::thread w(serverThreadHandler, newSocket);
    // w.join();
  }
  for(int c = 0; c < maxUsers; c++) {
    kill[c] = true;
    if(sockets[c] != NULL)
    workers[c]->join();
  }
}

void serverThreadWorker(int id, ServerSocket* socket, bool *kill, bool *done, string welcomeMessage) {
  //Run untill the main thread says to stop
  while(!(*kill)) {
    try {
      //Sleep, but check for an interrupt every 250 milliseconds
      boost::this_thread::sleep_for(boost::chrono::milliseconds{250});
    }
    catch (boost::thread_interrupted&) {
      //The thread was interrupted, so we must have a new client
      *done = false;

      socket->write(welcomeMessage);
      string clientIP = socket->clientIP();
      while(socket->isOpen()) {
        cout << clientIP << ": " << socket->read(1000);
      }

      cout << "Client disconnected" << endl;

      //Clean up the socket so there are no memory leaks
      delete socket;
      socket = (ServerSocket*)NULL;
      *done = true;
    }
  }
}

void serverThreadHandler(ServerSocket* socket) {
  //Communicate with the host until a disconnect happens
  cout << "Sending welcome message\n";
  socket->write("Welcome Message\n");
  //bool socketOpen = socket->isOpen();
  while(socket->isOpen()) {
      cout << socket->read(1000);
      //cout << "Finished reading socket, socket isOpen(): " << socket->isOpen() << std::endl;
      //socketOpen = socket->isOpen();
  }
  cout << "Disconnected from client" << endl;
  //Clean up the socket when the host disconnects
  delete socket;
}
