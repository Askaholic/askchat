#ifndef PARSER_H
#define PARSER_H

#include <string>
#include <vector>

class Parser {
  public:
    std::vector<std::string> tokenize(std::string);
    std::string removeSpace(std::string);
};

#endif
