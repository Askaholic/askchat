#include "Socket.h"
#include <iostream>
#include <unistd.h>

using std::cout;
using std::endl;

int main() {
  cout << "Initializing socket\n";
  ClientSocket cSocket(3705);
  cout << "Creating socket\n";
  cout << cSocket.create("localhost") << endl;
  cout << "Reading from socket\n";
  cout << cSocket.read(100);
  for(int c = 0; c < 10; c++) {
    cSocket.write("Hi Server, the world is here!\n");
    sleep(2);
  }
  cout << "Closing socket\n";
  cSocket.close();
}
